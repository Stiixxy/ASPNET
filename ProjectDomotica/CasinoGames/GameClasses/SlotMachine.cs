﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDomotica.CasinoGames.GameClasses {
	public class SlotMachineClass {

		string defaultPath = "~/CasinoGames/Assets/Slots/";
		public string Banana {
			get { return defaultPath + "Banana.png"; }
		} //ID: 0 Win: 1
		public string Lemon {
			get { return defaultPath + "Lemon.png"; }
		} //ID: 1 Win: 1.25
		public string Cherry {
			get { return defaultPath + "Cherry.png"; }
		} //ID: 2 Win: 1.5
		public string Citron {
			get { return defaultPath + "Citron.png"; }
		} //ID: 3 Win: 2
		public string Peach {
			get { return defaultPath + "Peach.png"; }
		} //ID: 4 Win: 2.5
		public string Melon {
			get { return defaultPath + "Melon.png"; }
		} //ID: 5 Win: 3
		public string Seven {
			get { return defaultPath + "7.png"; }
		} //ID: 6 Win: 10 
		public string Bar {
			get { return defaultPath + "Bar.png"; }
		} //ID: 7  Win: 100
		public string Bigwin {
			get { return defaultPath + "Bigwin.png"; }
		} //ID: 8  Win: Jackpot (increases with 10 credits every roll)
		public string getImagePath(int imageID) {
			switch (imageID) {
				case 0:
					return Banana;
				case 1:
					return Lemon;
				case 2:
					return Cherry;
				case 3:
					return Citron;
				case 4:
					return Peach;
				case 5:
					return Melon;
				case 6:
					return Seven;
				case 7:
					return Bar;
				case 8:
					return Bigwin;
				default:
					return Banana;
			}
		}

		int[] slots = { 0, 0, 0};
		double winPerc = 0.6;
		int[] percentages = {5000, 7000, 8000, 8750, 9250, 9500, 9600, 9650, 9660};//if won percentages of what is won, Sorted on id, from small to big. From 0 to 10.000;
		double[] wins = { 1, 1.25, 1.5, 2, 2.5, 3, 10, 100, 10, 100 };
		public int minBet = 1, maxBet = 200;
		Random r = new Random();
		bool hasPlayed;
		double lastWager;
		double lastWin;

		public SlotMachineClass() {
			
		}

		public void roll(double wager) {
			lastWager = wager;
			hasPlayed = true;
			if(r.NextDouble() < winPerc) {
				//We won somthing
				int roll = r.Next(0, percentages[8]);
				int winId = 0;
				if(roll < percentages[0]) {
					winId = 0;
				} else if (roll < percentages[1]) {
					winId = 1;
				} else if (roll < percentages[2]) {
					winId = 2;
				} else if (roll < percentages[3]) {
					winId = 3;
				} else if (roll < percentages[4]) {
					winId = 4;
				} else if (roll < percentages[5]) {
					winId = 5;
				} else if (roll < percentages[6]) {
					winId = 6;
				} else if (roll < percentages[7]) {
					winId = 7;
				} else if (roll < percentages[8]) {
					winId = 8;
				}
				slots[0] = slots[1] = slots[2] = winId;
				if (winId == 8) {
					lastWin = getJackpot();
					resetJackpot();
				} else {
					lastWin = lastWager * wins[winId];
				}
				if(HttpContext.Current.Application["jackpotslots"] == null) {
					HttpContext.Current.Application["jackpotslots"] = (double) r.Next(500, 2000);
				}
				HttpContext.Current.Application["jackpotslots"] = (double)HttpContext.Current.Application["jackpotslots"] + 10;
			} else {
				//We lost fill with random stuffs
				lastWin = 0;
				slots[0] = r.Next(0, 9);
				slots[1] = r.Next(0, 9);
				slots[2] = r.Next(0, 9);
				while(slots[0] == slots[1] && slots[1] == slots[2]) {
					slots[2] = r.Next(0, 9);
				}
			}
		}

		public double getJackpot() {
			if (HttpContext.Current.Application["jackpotslots"] == null) {
				resetJackpot();
			}
			return (double)HttpContext.Current.Application["jackpotslots"];
		}

		public void resetJackpot() {
			HttpContext.Current.Application["jackpotslots"] = (double)r.Next(500, 2000);
		}

		public bool getPlayed() {
			return hasPlayed;
		}

		public double getLastResult() {
			return lastWin;
		}

		public double getLastWager() {
			return lastWager;
		}

		public int[] getSlots() {
			return slots;
		}
	}
}