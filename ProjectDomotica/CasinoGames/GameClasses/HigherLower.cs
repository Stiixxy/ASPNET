﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDomotica.CasinoGames.GameClasses {
	public class HigherLowerClass {

		int lastNumber;
		int number;
		int min = 0, max = 100;
		double lastwager;
		bool hasPlayed;
		bool lastResult;
		public int minBet = 1, maxbet = 200;
		public double winPerc = 0.25;
		Random r = new Random();

		public HigherLowerClass() {
			number = r.Next(min, max);
		}

		public int getNumber() {
			return number;
		}

		public bool higher(double wager) {
			lastwager = wager;
			int next = r.Next(min, max);
			if(next > number) {
				lastNumber = number;
				hasPlayed = true;
				lastResult = true;
				number = next;
				return true;
			}else {
				lastNumber = number;
				hasPlayed = true;
				lastResult = false;
				number = next;
				return false;
			}
		}

		public bool lower(double wager) {
			lastwager = wager;
			int next = r.Next(min, max);
			if (next < number) {
				lastNumber = number;
				hasPlayed = true;
				lastResult = true;
				number = next;
				return true;
			} else {
				lastNumber = number;
				hasPlayed = true;
				lastResult = false;
				number = next;
				return false;
			}
		}

		public int getPrevious() {
			return lastNumber;
		}

		public bool getPlayed() {
			return hasPlayed;
		}

		public bool getLastResult() {
			return lastResult;
		}

		public double getLastWager() {
			return lastwager;
		}
	}
}