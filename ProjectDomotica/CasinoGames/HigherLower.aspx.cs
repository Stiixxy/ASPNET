﻿using ProjectDomotica.CasinoGames.GameClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.CasinoGames {
	public partial class HigherLower : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (Session["UserID"] == null) {
				Response.Redirect("~/Login/Login.aspx");
			}
			balanceHeader.InnerHtml = Global.replaceString("Balance: {balance}");

			if(Session["HigherLower"] == null) {
				Session["HigherLower"] = new HigherLowerClass();
			}
			HigherLowerClass game = (HigherLowerClass)Session["HigherLower"];

			if (!IsPostBack) {
				if (game.getPlayed()) {
					txtWager.Text = game.getLastWager().ToString();
				}else {
					txtWager.Text = game.minBet.ToString();
				} 
			}

			currentP.InnerHtml = "Current: " + game.getNumber();
			if(game.getPlayed()) {
				previousP.InnerHtml = "Previous: " + game.getPrevious();

				if (game.getLastResult()) {
					divResponse.Attributes["class"] = "alert alert-success";
					divResponse.InnerHtml = "<strong>Congratulations</strong> You won " + game.getLastWager() * game.winPerc + " credits!";
				}else {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Too bad</strong> You lost " + game.getLastWager()+ " credits!";
				}
			}
		}

		protected void btnHigher_Click(object sender, EventArgs e) {
			HigherLowerClass game = (HigherLowerClass)Session["HigherLower"];
			double wager = 0;
			try {
				wager = double.Parse(txtWager.Text);
				if (wager < game.minBet || wager > game.maxbet || wager > Global.getBalance((int)Session["UserID"])) {
					return;
				}
			} catch (Exception) {
				return;
			}
			
			game.higher(wager);
			if (game.getLastResult()) {
				Global.addBalance((int)Session["UserID"], game.getLastWager() * game.winPerc);
			} else {
				Global.addBalance((int)Session["UserID"], game.getLastWager() * -1);
			}
			Response.Redirect("~/CasinoGames/HigherLower.aspx");
		}

		protected void btnLower_Click(object sender, EventArgs e) {
			HigherLowerClass game = (HigherLowerClass)Session["HigherLower"];
			double wager = 0;
			try {
				wager = double.Parse(txtWager.Text);
				if(wager < game.minBet || wager > game.maxbet || wager > Global.getBalance((int)Session["UserID"])) {
					return;
				}
			} catch (Exception) {
				return;
			}
			
			game.lower(double.Parse(txtWager.Text));
			if (game.getLastResult()) {
				Global.addBalance((int) Session["UserID"], game.getLastWager() * game.winPerc);
			}else {
				Global.addBalance((int)Session["UserID"], game.getLastWager() * -1);
			}
			Response.Redirect("~/CasinoGames/HigherLower.aspx");
		}

		protected void txtWager_TextChanged(object sender, EventArgs e) {
			try {
				double wager = double.Parse(txtWager.Text);
				HigherLowerClass game = (HigherLowerClass)Session["HigherLower"];

				if (wager < game.minBet) {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Error </strong> the minimum bet is " + game.minBet + "!";
				} else if (wager > game.maxbet) {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Error </strong> the maximum bet is " + game.maxbet + "!";
				} else if (wager > Global.getBalance((int)Session["UserID"])) {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Error </strong> your current balance is to low!";
				}

			} catch (Exception) {
				divResponse.Attributes["class"] = "alert alert-danger";
				divResponse.InnerHtml = "<strong>Error </strong> please enter a valid number!";
			}
		}
	}
}