﻿using ProjectDomotica.CasinoGames.GameClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.CasinoGames {
	public partial class SlotMachine : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (Session["UserID"] == null) {
				Response.Redirect("~/Login/Login.aspx");
			}
			balanceHeader.InnerHtml = Global.replaceString("Balance: {balance}");
			

			if (Session["slots"] == null) {
				Session["slots"] = new SlotMachineClass();
			}
			SlotMachineClass game = (SlotMachineClass)Session["slots"];
			jackpotHeader.InnerHtml = "Jackpot: " + game.getJackpot();

			if (!IsPostBack) {
				if (game.getPlayed()) {
					txtWager.Text = game.getLastWager().ToString();
				} else {
					txtWager.Text = game.minBet.ToString();
				}
			}

			if (game.getPlayed()) {
				if(game.getLastResult() > 0) {
					divResponse.Attributes["class"] = "alert alert-success";
					divResponse.InnerHtml = "<strong>Congratulations</strong> You won " + game.getLastResult() + " credits!";
				}else {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Too bad</strong> You lost " + game.getLastWager() + " credits!";
				}
			}
			setSlots();
		}

		protected void setSlots() {
			if (Session["slots"] == null) {
				Session["slots"] = new SlotMachineClass();
			}
			SlotMachineClass game = (SlotMachineClass)Session["slots"];

			slot1.Src = game.getImagePath(game.getSlots()[0]);
			slot2.Src = game.getImagePath(game.getSlots()[1]);
			slot3.Src = game.getImagePath(game.getSlots()[2]);
		}

		protected void txtWager_TextChanged(object sender, EventArgs e) {
			try {
				double wager = double.Parse(txtWager.Text);
				SlotMachineClass game = (SlotMachineClass)Session["slots"];

				if (wager < game.minBet) {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Error </strong> the minimum bet is " + game.minBet + "!";
				} else if (wager > game.maxBet) {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Error </strong> the maximum bet is " + game.maxBet + "!";
				} else if (wager > Global.getBalance((int)Session["UserID"])) {
					divResponse.Attributes["class"] = "alert alert-danger";
					divResponse.InnerHtml = "<strong>Error </strong> your current balance is to low!";
				}

			} catch (Exception) {
				divResponse.Attributes["class"] = "alert alert-danger";
				divResponse.InnerHtml = "<strong>Error </strong> please enter a valid number!";
			}
		}

		protected void btnRoll_Click(object sender, EventArgs e) {
			SlotMachineClass game = (SlotMachineClass)Session["slots"];
			double wager = 0;
			try {
				wager = double.Parse(txtWager.Text);
				if (wager < game.minBet || wager > game.maxBet || wager > Global.getBalance((int)Session["UserID"])) {
					return;
				}
			} catch (Exception) {
				return;
			}

			game.roll(wager);
			Global.addBalance((int)Session["UserID"], game.getLastResult() - game.getLastWager());
			Response.Redirect("~/CasinoGames/SlotMachine.aspx");
		}
	}
}