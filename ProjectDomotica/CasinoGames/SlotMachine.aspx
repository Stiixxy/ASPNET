﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMasterPage.Master" AutoEventWireup="true" CodeBehind="SlotMachine.aspx.cs" Inherits="ProjectDomotica.CasinoGames.SlotMachine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<div style="width: 50%; margin: 10% auto auto auto; border: 3px solid black; padding: 5px 5px 5px 5px; border-radius: 10px;">

		<h4 style="width: 100%; text-align: center; margin: 20px auto auto auto;">Higher lower</h4>
		<h5 id="balanceHeader" style="width: 100%; text-align: center; margin: 10px auto auto auto;" runat="server">Balance: </h5>
		<h5 id="jackpotHeader" style="width: 100%; text-align: center; margin: 10px auto auto auto;" runat="server">Balance: </h5>

		<div style="margin: 5% auto auto auto; text-align: center;">
			<img src="Assets/Slots/Cherry.png" id="slot1" runat="server" style="height: 10%; width: 10%;" />
			<img src="Assets/Slots/Cherry.png" id="slot2" runat="server" style="height: 10%; width: 10%;" />
			<img src="Assets/Slots/Cherry.png" id="slot3" runat="server" style="height: 10%; width: 10%;" />
		</div>
		<div style="margin: 5% auto auto auto; text-align: center">
			<p>
				Wager:<asp:TextBox ID="txtWager" runat="server" Style="margin-left: 1%" AutoPostBack="true" OnTextChanged="txtWager_TextChanged"></asp:TextBox>
			</p>
		</div>
		<div style="margin: auto auto auto auto; text-align:center;">
			<asp:Button ID="btnRoll" class="btn btn-danger" Style="width: 30%; padding: 5px 10px 5px 10px" Text="Roll" runat="server" OnClick="btnRoll_Click" />
		</div>

		<div class="alert" role="alert" id="divResponse" runat="server" style="width:100%; margin: 5% auto auto auto;"></div>
	</div>

</asp:Content>
