﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMasterPage.Master" AutoEventWireup="true" CodeBehind="HigherLower.aspx.cs" Inherits="ProjectDomotica.CasinoGames.HigherLower" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="GameDiv" runat="server">


		<div style="width: 50%; margin: 10% auto auto auto; border: 3px solid black; padding: 5px 5px 5px 5px; border-radius: 10px;">

			<h4 style="width: 100%; text-align: center; margin: 20px auto auto auto;">Higher lower</h4>
			<h5 id="balanceHeader" style="width: 100%; text-align: center; margin: 10px auto auto auto;" runat="server">Balance: </h5>

			<p style="width: 100%; text-align: center; margin: 30px auto auto auto" id="currentP" runat="server">Current: </p>
			<p style="width: 100%; text-align: center; margin: auto auto auto auto" id="previousP" runat="server"></p>
			<div style="margin: 30px auto auto auto; text-align:center;">
				<p>Wager: <asp:TextBox ID="txtWager" runat="server" AutoPostBack="true" OnTextChanged="txtWager_TextChanged"></asp:TextBox></p>
			</div>
			<div style="margin: 10px auto 10px auto; text-align: center">
				<asp:Button ID="btnHigher" class="btn btn-danger" Style="width: 30%; padding: 5px 10px 5px 10px; margin-right: 5%" runat="server" Text="Higher" OnClick="btnHigher_Click" />
				<asp:Button id="btnLower" class="btn btn-success" Style="width: 30%; padding: 5px 10px 5px 10px" runat="server" Text="Lower" OnClick="btnLower_Click" />
			</div>

			<div class="alert" role="alert" id="divResponse" runat="server" style="width:100%; margin: 5% auto auto auto;"></div>

		</div>


	</div>
</asp:Content>
