﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;

namespace ProjectDomotica {
    public class ViewableGrid {

        public System.Web.UI.HtmlControls.HtmlGenericControl grid { get; set; }
        public string tableID { get; set; }

        public void generate (int[] _blocks, int _width, int _height, int _distance) {
            int HorizontalBlocks = _blocks[0];
            int VerticalBlocks = _blocks[1];
            int top = 0;
            int left = 0;
            int totalWidth = 0;
            int totalHeight = 0;

            for (int i = 0; i < VerticalBlocks; i++) {
                for (int x = 0; x < HorizontalBlocks; x++) {
                    System.Web.UI.HtmlControls.HtmlGenericControl Div = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");

                    Div.ID = "Box_" + x + "_" + i;
                    Div.Style.Add(HtmlTextWriterStyle.Width, _width.ToString() + "px");
                    Div.Style.Add(HtmlTextWriterStyle.Height, _height.ToString() + "px");
                    Div.Style.Add(HtmlTextWriterStyle.Top, top.ToString() + "px");
                    Div.Style.Add(HtmlTextWriterStyle.Left, left.ToString() + "px");
                    Div.Attributes.Add("class", "block");
                    Div.Attributes.Add("runat", "server");
                    Div.Attributes.Add("onclick", "div_click(this.id)");
                    Div.Attributes.Add("onmouseover", "div_mouseOver(this.id)");
                    Div.Attributes.Add("onmouseout", "div_mouseOut(this.id)");

                    grid.Controls.Add(Div);

                    left += _width + _distance;
                }

                totalWidth = left;

                left = 0;
                top += _height + _distance;
            }

            totalHeight = top;

            grid.Style.Add(HtmlTextWriterStyle.MarginLeft, "-" + (totalWidth / 2) + "px");
            grid.Style.Add(HtmlTextWriterStyle.MarginTop, "-" + (totalHeight / 2) + "px");
        }
    }
}