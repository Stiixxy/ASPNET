﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;

namespace ProjectDomotica {
	public class DaHausControl {
		string ip = "127.0.0.1";
		int port = 11000;
		TcpClient client = new TcpClient();
		NetworkStream stream;
		bool connected = false;

		public bool[] lights = { false, false, false, false, false };
		public bool[] windows = { true, true };
		public double temp = 21.0;

		public void connect() {
			try {
				client.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
				stream = client.GetStream();
				connected = true;
			} catch (Exception) { }
		}

		public void closeConnection() {
			send("exit");
			waitForResponse();
			client.Close();
			client = new TcpClient();
			connected = false;
		}

		public void setTemp(string temp) {
			send("heater " + temp);
			this.temp = double.Parse(temp);
			waitForResponse();
		}

		public string toggleLight(string number) {
			send("lamp " + number);
			string response = waitForResponse();
			if (!lights[int.Parse(number)]) {//Lamp is off
				send("lamp " + number + " on");
				lights[int.Parse(number)] = true;
			} else {//Lamp is on
				lights[int.Parse(number)] = false;
				send("lamp " + number + " Off");
			}
			response = waitForResponse();
			return response;
		}

		public string toggleWindow(string number) {
			send("window " + number);
			string response = waitForResponse();
			if (response.Contains("Closed")) {//Lamp is off
				send("window " + number + " open");
			} else {//Lamp is on
				send("window " + number + " close");
			}
			response = waitForResponse();
			return response;
		}

		public void send(string command) {
			string sendString = command + "\n";
			byte[] bytes = Encoding.ASCII.GetBytes(sendString);
			stream.Write(bytes, 0, bytes.Length);
		}

		public string waitForResponse() {
			string response = "";
			while (response.Equals("")) {
				response = getResponse();
			}
			return response;
		}

		public string getResponse() {
			string message = "";
			if (stream.CanRead) {
				byte[] buffer = new byte[1024];
				int bytesRead = 0;

				do {
					bytesRead = stream.Read(buffer, 0, buffer.Length);
					message += Encoding.ASCII.GetString(buffer, 0, buffer.Length);
				} while (stream.DataAvailable);
				return message;
			}
			return message;
		}

		public bool getLightState(string lightID) {
			string message = "lamp " + lightID + "\n";
			byte[] bytes = Encoding.ASCII.GetBytes(message);
			stream.Write(bytes, 0, bytes.Length);

			string response = waitForResponse();
			if (response.Contains("On")) {
				return true;
			} else {
				return false;
			}
		}

		public void setLight(string lightID, bool enabled) {
			if (enabled) {
				send("lamp " + lightID + " on");
				lights[int.Parse(lightID)] = true;
			} else {
				send("lamp " + lightID + " Off");
				lights[int.Parse(lightID)] = false;
			}
			waitForResponse();
			
		}

		public void setWindow(string windowID, bool enabled) {
			if (enabled) {
				send("Window " + windowID + " open");
				windows[int.Parse(windowID)] = true;
			} else {
				send("Window " + windowID + " close");
				windows[int.Parse(windowID)] = false;
			}
			waitForResponse();
		}

		public bool getConnected() {
			return connected;
		}
	}
}