﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DaHausControl.aspx.cs" Inherits="ProjectDomotica.DaHaus.DaHausControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<asp:Label ID="LblWelcomeMessage" runat="server" Text="-"></asp:Label>
			<asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
			<asp:Button ID="BtnAdminControl" runat="server" Text="Admin Control" style="float:right" OnClick="BtnAdminControl_Click"/>
		</div>
		<div>
			<asp:CheckBox ID="cbLight0" runat="server" AutoPostBack="True" OnCheckedChanged="cbLight0_CheckedChanged" Text="Light 0" />
			<asp:CheckBox ID="cbLight1" runat="server" AutoPostBack="True" OnCheckedChanged="cbLight1_CheckedChanged" Text="Light 1" />
			<asp:CheckBox ID="cbLight2" runat="server" AutoPostBack="True" OnCheckedChanged="cbLight2_CheckedChanged" Text="Light 2" />
			<asp:CheckBox ID="cbLight3" runat="server" AutoPostBack="True" OnCheckedChanged="cbLight3_CheckedChanged" Text="Light 3" />
			<asp:CheckBox ID="cbLight4" runat="server" AutoPostBack="True" OnCheckedChanged="cbLight4_CheckedChanged" Text="Light 4" />
		</div>
		<div>
			<asp:CheckBox ID="cbWindow0" runat="server" AutoPostBack="True" OnCheckedChanged="cbWindow0_CheckedChanged" Text="Window 0" />
			<asp:CheckBox ID="cbWindow1" runat="server" AutoPostBack="True" OnCheckedChanged="cbWindow1_CheckedChanged" Text="Window 1" />
		</div>
		<div>
			<asp:TextBox ID="txtHeater" runat="server" AutoPostBack="True" OnTextChanged="txtHeater_TextChanged"></asp:TextBox>
			<asp:Button ID="BtnAddSmall" runat="server" Text="+0.1" OnClick="BtnAddSmall_Click" />
			<asp:Button ID="BtnRemoveSmall" runat="server" Text="-0.1" OnClick="BtnRemoveSmall_Click" />
			<asp:Button ID="BtnAddBig" runat="server" Text="+1" OnClick="BtnAddBig_Click" />
			<asp:Button ID="BtnRemoveBig" runat="server" Text="-1" OnClick="BtnRemoveBig_Click" />
		</div>
	</form>
</body>
</html>

