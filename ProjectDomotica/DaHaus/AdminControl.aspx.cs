﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ProjectDomotica.DaHaus {
	public partial class AdminControl : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (Session["UserID"] == null) {
				Response.Redirect("/MasterPageTest.aspx");
			}
			if (!Global.isAdmin((int)Session["UserID"])) {
				Response.Redirect("/MasterPageTest.aspx");
			}
			if (!IsPostBack) {
				ddUsers.Items.Clear();
				MySqlDataReader reader = Global.getSqlConnection().executeSQLCommand(new MySqlCommand("SELECT username, userid FROM users"));

				ddUsers.Items.Add(new ListItem("None", "None"));
				if (reader.HasRows) {
					while (reader.Read()) {
						ddUsers.Items.Add(new ListItem(reader.GetString(0), reader.GetInt32(1).ToString()));
					}
					reader.Close();
				}
			}
		}

		protected void ddUsers_SelectedIndexChanged(object sender, EventArgs e) {
			lblResponse.Text = "Selected user: " + ddUsers.SelectedItem.Text;
			if (ddUsers.SelectedValue.Equals("None")) {
				controls.Style.Add("visibility", "hidden");
			} else {
				controls.Style.Remove("visibility");
			}
		}

		protected void BtnSumbit_Click(object sender, EventArgs e) {
			string permissionString = "";
			permissionString += (Light0Disabled.Checked) ? "0" : "";
			permissionString += (Light0Visable.Checked) ? "1" : "";
			permissionString += (Light0Enabled.Checked) ? "2" : "";

			permissionString += (Light1Disabled.Checked) ? "0" : "";
			permissionString += (Light1Visable.Checked) ? "1" : "";
			permissionString += (Light1Enabled.Checked) ? "2" : "";

			permissionString += (Light2Disabled.Checked) ? "0" : "";
			permissionString += (Light2Visable.Checked) ? "1" : "";
			permissionString += (Light2Enabled.Checked) ? "2" : "";

			permissionString += (Light3Disabled.Checked) ? "0" : "";
			permissionString += (Light3Visable.Checked) ? "1" : "";
			permissionString += (Light3Enabled.Checked) ? "2" : "";

			permissionString += (Light4Disabled.Checked) ? "0" : "";
			permissionString += (Light4Visable.Checked) ? "1" : "";
			permissionString += (Light4Enabled.Checked) ? "2" : "";

			permissionString += (Window0Disabled.Checked) ? "0" : "";
			permissionString += (Window0Visable.Checked) ? "1" : "";
			permissionString += (Window0Enabled.Checked) ? "2" : "";

			permissionString += (Window1Disabled.Checked) ? "0" : "";
			permissionString += (Window1Visable.Checked) ? "1" : "";
			permissionString += (Window1Enabled.Checked) ? "2" : "";

			permissionString += (HeaterDisabled.Checked) ? "0" : "";
			permissionString += (HeaterVisable.Checked) ? "1" : "";
			permissionString += (HeaterEnabled.Checked) ? "2" : "";

			MySqlDataReader reader;
			MySqlCommand command = new MySqlCommand("SELECT userid FROM `permissions` WHERE userid =? ");
			command.Parameters.Add("ddUsers.SelectedValue");
			reader = Global.SqlConnection.executeSQLCommand(command);
			if (reader.HasRows) {
				reader.Close();
				command = new MySqlCommand("UPDATE permissions SET dahaus = ? WHERE userid = ?;");
				command.Parameters.Add(permissionString);
				command.Parameters.Add(ddUsers.SelectedValue);
				reader = Global.getSqlConnection().executeSQLCommand(command);
				if (reader.RecordsAffected > 0) {
					lblSumbitResponse.InnerText = "The permissions of " + ddUsers.SelectedItem.Text + " are updated!";
				} else {
					lblSumbitResponse.InnerText = "Nothing changed!";
				}
				reader.Close();
			} else {
				reader.Close();
				command = new MySqlCommand("INSERT INTO permissions(`userid`, `dahaus`) VALUES(?, ?);");
				command.Parameters.Add(ddUsers.SelectedValue);
				command.Parameters.Add(permissionString);
				if(reader.RecordsAffected > 0) {
					lblSumbitResponse.InnerText = "The permissions of " + ddUsers.SelectedItem.Text + " are updated!";
				}else {
					lblSumbitResponse.InnerText = "Nothing changed!";
				}
				reader.Close();
			}
		}

		protected void BtnGoBack_Click(object sender, EventArgs e) {
			Server.Transfer("DaHausControl.aspx");
		}
	}
}