﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.DaHaus {
	public partial class DaHausControl : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			string permissionString = "00000000";
			int userID = 0;
			if(Session["UserID"] == null) {
				Server.Transfer("~/Login/Login.aspx");
			}else {
				userID = (int)Session["UserID"];
			}
			
			LblWelcomeMessage.Text = "Welcome to the DaHaus control page " + Global.getUsername(userID);
			LblWelcomeMessage.BackColor = Color.White;
			MySqlCommand command = new MySqlCommand("SELECT dahaus FROM permissions WHERE userid = ?;");
			command.Parameters.Add(userID);
			MySqlDataReader reader = Global.getSqlConnection().executeSQLCommand(command);
			if (reader.HasRows) {
				reader.Read();

				permissionString = reader.GetString(0);
			}
			reader.Close();
			if (!IsPostBack) {
				cbLight0.Checked = Global.getDaHaus().lights[0];
				cbLight1.Checked = Global.getDaHaus().lights[1];
				cbLight2.Checked = Global.getDaHaus().lights[2];
				cbLight3.Checked = Global.getDaHaus().lights[3];
				cbLight4.Checked = Global.getDaHaus().lights[4];

				cbWindow0.Checked = Global.DaHaus.windows[0];
				cbWindow1.Checked = Global.DaHaus.windows[1];

				txtHeater.Text = Global.DaHaus.temp.ToString();
			} 

			//Check if you can edit
			cbLight0.Enabled = permissionString.Substring(0, 1).Equals("2");
			cbLight1.Enabled = permissionString.Substring(1, 1).Equals("2");
			cbLight2.Enabled = permissionString.Substring(2, 1).Equals("2");
			cbLight3.Enabled = permissionString.Substring(3, 1).Equals("2");
			cbLight4.Enabled = permissionString.Substring(4, 1).Equals("2");

			cbWindow0.Enabled = permissionString.Substring(5, 1).Equals("2");
			cbWindow1.Enabled = permissionString.Substring(6, 1).Equals("2");

			BtnAddBig.Enabled = permissionString.Substring(7, 1).Equals("2");
			BtnAddSmall.Enabled = permissionString.Substring(7, 1).Equals("2");
			BtnRemoveBig.Enabled = permissionString.Substring(7, 1).Equals("2");
			BtnRemoveSmall.Enabled = permissionString.Substring(7, 1).Equals("2");

			txtHeater.Enabled = permissionString.Substring(7, 1).Equals("2");

			//Check if it needs to be hidden
			cbLight0.Visible = !permissionString.Substring(0, 1).Equals("1");
			cbLight1.Visible = !permissionString.Substring(1, 1).Equals("1");
			cbLight2.Visible = !permissionString.Substring(2, 1).Equals("1");
			cbLight3.Visible = !permissionString.Substring(3, 1).Equals("1");
			cbLight4.Visible = !permissionString.Substring(4, 1).Equals("1");

			cbWindow0.Visible = !permissionString.Substring(5, 1).Equals("1");
			cbWindow1.Visible = !permissionString.Substring(6, 1).Equals("1");

			BtnAddBig.Visible = !permissionString.Substring(7, 1).Equals("1");
			BtnAddSmall.Visible = !permissionString.Substring(7, 1).Equals("1");
			BtnRemoveBig.Visible = !permissionString.Substring(7, 1).Equals("1");
			BtnRemoveSmall.Visible = !permissionString.Substring(7, 1).Equals("1");

			txtHeater.Visible = !permissionString.Substring(7, 1).Equals("1");

			if (!Global.DaHaus.getConnected()) {
				LblWelcomeMessage.Text = "WARNING! You are not connected to DaHaus. Please Startup DaHaus.";
				LblWelcomeMessage.BackColor = Color.Red;
			}
			
		}

		protected void cbLight0_CheckedChanged(object sender, EventArgs e) {
			Global.getDaHaus().setLight("0", cbLight0.Checked);
		}

		protected void cbLight1_CheckedChanged(object sender, EventArgs e) {
			Global.getDaHaus().setLight("1", cbLight1.Checked);
		}

		protected void cbLight2_CheckedChanged(object sender, EventArgs e) {
			Global.getDaHaus().setLight("2", cbLight2.Checked);
		}

		protected void cbLight3_CheckedChanged(object sender, EventArgs e) {
			Global.getDaHaus().setLight("3", cbLight3.Checked);
		}

		protected void cbLight4_CheckedChanged(object sender, EventArgs e) {
			Global.getDaHaus().setLight("4", cbLight4.Checked);
		}

		protected void cbWindow0_CheckedChanged(object sender, EventArgs e) {
			Global.DaHaus.setWindow("0", cbWindow0.Checked);
		}

		protected void cbWindow1_CheckedChanged(object sender, EventArgs e) {
			Global.DaHaus.setWindow("1", cbWindow1.Checked);
		}

		protected void btnRefresh_Click(object sender, EventArgs e) {
			Server.Transfer("DaHausControl.aspx");
		}

		protected void BtnAddSmall_Click(object sender, EventArgs e) {
			double curTemp = double.Parse(txtHeater.Text);
			curTemp += 0.1;
			txtHeater.Text = curTemp.ToString();
			Global.DaHaus.setTemp(curTemp.ToString());
		}

		protected void BtnRemoveSmall_Click(object sender, EventArgs e) {
			double curTemp = double.Parse(txtHeater.Text);
			curTemp -= 0.1;
			txtHeater.Text = curTemp.ToString();
			Global.DaHaus.setTemp(curTemp.ToString());
		}

		protected void BtnAddBig_Click(object sender, EventArgs e) {
			double curTemp = double.Parse(txtHeater.Text);
			curTemp += 1.0;
			txtHeater.Text = curTemp.ToString();
			Global.DaHaus.setTemp(curTemp.ToString());
		}

		protected void BtnRemoveBig_Click(object sender, EventArgs e) {
			double curTemp = double.Parse(txtHeater.Text);
			curTemp -= 1.0;
			txtHeater.Text = curTemp.ToString();
			Global.DaHaus.setTemp(curTemp.ToString());
		}

		protected void txtHeater_TextChanged(object sender, EventArgs e) {
			try {
				double temp = double.Parse(txtHeater.Text);
				Global.DaHaus.setTemp(txtHeater.Text);
			} catch (Exception) { }
		}

		protected void BtnAdminControl_Click(object sender, EventArgs e) {
			Server.Transfer("Admincontrol.aspx");
		}
	}
}