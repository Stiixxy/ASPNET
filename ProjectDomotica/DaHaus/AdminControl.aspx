﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminControl.aspx.cs" Inherits="ProjectDomotica.DaHaus.AdminControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<asp:Label ID="lblWelcome" runat="server" Text="Welcome to the DaHaus admin page."></asp:Label>
			<asp:Button ID="BtnGoBack" runat="server" Text="DaHaus control" style="float:right" OnClick="BtnGoBack_Click"/>
		</div>
		<div>
			<asp:Label ID="lblSelectUser" runat="server" Text="Please select an user:"></asp:Label>
			<asp:DropDownList ID="ddUsers" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddUsers_SelectedIndexChanged">
			</asp:DropDownList>
		</div>
		<div>
			<asp:Label ID="lblResponse" runat="server" Text="Selected user: None"></asp:Label>
		</div>
		<div id="controls" runat="server" style="visibility: hidden">
			<div id="LightControl" runat="server" style="padding-top: 20px">
				<table>
					<tr>
						<th>Light</th>
						<th>Enabled</th>
						<th>Visable</th>
						<th>Disabled</th>
					</tr>
					<tr>
						<th>0</th>
						<th>
							<asp:RadioButton ID="Light0Enabled" runat="server" GroupName="Lamp0" />
						</th>
						<th>
							<asp:RadioButton ID="Light0Visable" runat="server" GroupName="Lamp0" />
						</th>
						<th>
							<asp:RadioButton ID="Light0Disabled" runat="server" Checked="True" GroupName="Lamp0" />
						</th>
					</tr>
					<tr>
						<th>1</th>
						<th>
							<asp:RadioButton ID="Light1Enabled" runat="server" GroupName="Lamp1" />
						</th>
						<th>
							<asp:RadioButton ID="Light1Visable" runat="server" GroupName="Lamp1" />
						</th>
						<th>
							<asp:RadioButton ID="Light1Disabled" runat="server" Checked="True" GroupName="Lamp1" />
						</th>
					</tr>
					<tr>
						<th>2</th>
						<th>
							<asp:RadioButton ID="Light2Enabled" runat="server" GroupName="Lamp2" />
						</th>
						<th>
							<asp:RadioButton ID="Light2Visable" runat="server" GroupName="Lamp2" />
						</th>
						<th>
							<asp:RadioButton ID="Light2Disabled" runat="server" Checked="True" GroupName="Lamp2" />
						</th>
					</tr>
					<tr>
						<th>3</th>
						<th>
							<asp:RadioButton ID="Light3Enabled" runat="server" GroupName="Lamp3" />
						</th>
						<th>
							<asp:RadioButton ID="Light3Visable" runat="server" GroupName="Lamp3" />
						</th>
						<th>
							<asp:RadioButton ID="Light3Disabled" runat="server" Checked="True" GroupName="Lamp3" />
						</th>
					</tr>
					<tr>
						<th>4</th>
						<th>
							<asp:RadioButton ID="Light4Enabled" runat="server" GroupName="Lamp4" />
						</th>
						<th>
							<asp:RadioButton ID="Light4Visable" runat="server" GroupName="Lamp4" />
						</th>
						<th>
							<asp:RadioButton ID="Light4Disabled" runat="server" Checked="True" GroupName="Lamp4" />
						</th>
					</tr>
				</table>
			</div>
			<div id="WindowControl" runat="server" style="padding-top: 20px">
				<table>
					<tr>
						<th>Windows</th>
						<th>Enabled</th>
						<th>Visable</th>
						<th>Disabled</th>
					</tr>
					<tr>
						<th>0</th>
						<th>
							<asp:RadioButton ID="Window0Enabled" runat="server" GroupName="Window0" />
						</th>
						<th>
							<asp:RadioButton ID="Window0Visable" runat="server" GroupName="Window0" />
						</th>
						<th>
							<asp:RadioButton ID="Window0Disabled" runat="server" Checked="True" GroupName="Window0" />
						</th>
					</tr>
					<tr>
						<th>1</th>
						<th>
							<asp:RadioButton ID="Window1Enabled" runat="server" GroupName="Window1" />
						</th>
						<th>
							<asp:RadioButton ID="Window1Visable" runat="server" GroupName="Window1" />
						</th>
						<th>
							<asp:RadioButton ID="Window1Disabled" runat="server" Checked="True" GroupName="Window1" />
						</th>
					</tr>
				</table>
			</div>
			<div id="HeaterControl" runat="server" style="padding-top:20px; margin-bottom:20px">
				<h4>Heater</h4>
				<table style="margin-top:-20px">
					<tr>
						<th>Enabled</th>
						<th>Visable</th>
						<th>Disabled</th>
					</tr>
					<tr>
						<th>
							<asp:RadioButton ID="HeaterEnabled" runat="server" GroupName="Heater" />
						</th>
						<th>
							<asp:RadioButton ID="HeaterVisable" runat="server" GroupName="Heater" />
						</th>
						<th>
							<asp:RadioButton ID="HeaterDisabled" runat="server" Checked="True" GroupName="Heater" />
						</th>
					</tr>
				</table>
			</div>
			<div>
				<asp:Button ID="BtnSumbit" runat="server" Text="Sumbit changes" OnClick="BtnSumbit_Click" />
			</div>
			<div id="lblSumbitResponse" runat="server" style="padding-top:20px;">

			</div>
		</div>
	</form>
</body>
</html>
