﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ProjectDomotica {
	public class DatabaseConnection {

		static string server = "jordipoel.nl";
		static string database = "prdomotica";
		static string username = "stijn";
		static string password = "@Domotica!";
		static string port = "22005";
		static string connectionString = "SERVER=" + server + ";port=" + port + ";DATABASE=" + database + ";UID=" + username + ";PASSWORD=" + password + ";";
		MySqlConnection connection;

		public bool openConnection() {
			if(connection == null) {
				connection = new MySqlConnection(connectionString);
			}
			if (connection.State != ConnectionState.Open) {
				connection.Open();

				if (connection.State == ConnectionState.Open) {
					return true;
				} else {
					return false;
				}
			}else {
				return true;
			}
		}

		public void closeConnection() {
			connection.Close();
		}

		public bool checkConnection() {
			if (connection.State == ConnectionState.Open) {
				return true;
			} else {
				return false;
			}
		}

		public MySqlDataReader executeSQLCommand(MySqlCommand sqlCommand) {
			//openConnection();
			if (checkConnection()) {
				//MySqlCommand sqlCommand = new MySqlCommand(command, connection);
				sqlCommand.Connection = connection;
				MySqlDataReader reader = sqlCommand.ExecuteReader();
				return reader;
			}else {
				closeConnection();
				return null;
			}
		}
	}
}