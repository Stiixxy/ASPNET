﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica {
    public partial class Main : System.Web.UI.Page {

        ViewableGrid vg = new ViewableGrid();

        protected void Page_Load (object sender, EventArgs e) {

            if (!this.IsPostBack) {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "setSizeToHiddenObj()", true);
                Server.Transfer("Main.aspx");

            } else {
                int[] blocks = { 10, 4 };

                vg.grid = GridContainer;
                vg.tableID = GridContainer.ID;
                vg.generate(blocks, 100, 100, 10);

                //lbl_test.Text = hiddenValue.Style["width"];
            }

        }
    }
}