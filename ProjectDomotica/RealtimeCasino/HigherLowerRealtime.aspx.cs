﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.RealtimeCasino {
	public partial class HigherLowerRealtime : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if(Session["UserID"] == null) {
				Response.Redirect("MasterPageTest.aspx");
			}
			if (Application["HigherLower"] == null) {
				Application["HigherLower"] = new HigherLowerGame();
			}
			HigherLowerGame game = (HigherLowerGame)Application["HigherLower"];
			//if (!game.isConnected((int)Session["UserID"])){
				string secretHash = game.getAuthHash((int)Session["UserID"]);
				Response.Write("<script> var authHash = \"" + secretHash + "\"; var connected=false;</script>");
			//}else {
			//	Response.Write("<script>var connected=true;</script>");
			//}
		}
	}
}