﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMasterPage.Master" AutoEventWireup="true" CodeBehind="Minesweeper.aspx.cs" Inherits="ProjectDomotica.RealtimeCasino.MinesweeperPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<div style="width: 50%; height:auto; margin: 10% auto auto auto; border: 3px solid black; padding: 5px 5px 5px 5px; border-radius: 10px;">
		<script src="/Scripts/jquery-1.6.4.min.js"></script>
		<script src="/Scripts/jquery.signalR-1.2.2.min.js"></script>
		<script src="/signalr/hubs"></script>
		<script src="/RealtimeCasino/Minesweeper/Minesweeper.js"></script>

		<h4 style="width: 100%; text-align: center; margin: 20px auto auto auto;">Higher lower</h4>
		<h5 id="balanceHeader" style="width: 100%; text-align: center; margin: 2% auto auto auto;" runat="server">Balance: </h5>

		<div style="margin-top: 5%;">
			<div id="startGame" style="text-align: center; width: 100%; visibility: visible">
				<div>
					<p>
						<strong>Wager: </strong>
						<asp:TextBox runat="server" ID="txtWager"></asp:TextBox>						
					</p>
					<p>
						<strong>Bombs</strong>
						<asp:TextBox runat="server" ID="txtBombs"></asp:TextBox>
					</p>
					<div id="startgameBtn" class="btn btn-success" onclick="playGameClick()"><strong>Play Game</strong></div>
				</div>
			</div>

			<div id="runningGame" style="visibility: hidden; margin-top: -10%">
				<div id="grid" style="text-align: center; margin: auto auto auto auto; width: 60%; height: 50%; display: inline-block;">
					<div>
						<div onclick="onGridClick(0,0)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(1,0)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(2,0)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(3,0)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(4,0)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
					</div>
					<div>
						<div onclick="onGridClick(0,1)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(1,1)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(2,1)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(3,1)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(4,1)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
					</div>
					<div>
						<div onclick="onGridClick(0,2)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(1,2)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(2,2)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(3,2)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(4,2)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
					</div>
					<div>
						<div onclick="onGridClick(0,3)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(1,3)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(2,3)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(3,3)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(4,3)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
					</div>
					<div>
						<div onclick="onGridClick(0,4)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(1,4)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(2,4)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(3,4)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
						<div onclick="onGridClick(4,4)" style="width: 15%; height: 0; padding-bottom: 15%; border: 1px solid black; display: inline-block;"></div>
					</div>
				</div>
				<div id="controls" style="display: inline-block; width:30%; vertical-align: top; margin-top: 10%;">
					<h4 id="nextClick">Next click: +41</h4>
					<div id="cashoutBtn" class="btn btn-warning" onclick="cashout()" style="width: 100%">
						<strong>Cashout:</strong><br />
						1500 cr
					</div>
				</div>
				<div id="endgameBtn" style="margin: 10px auto auto auto; width: 100%; visibility: hidden; text-align:center">
					<div style="display: inline-block; width:20%; margin-right:5%;" class="btn btn-danger" onclick="endGameClick()"><strong>Close game</strong></div>
					<div style="display: inline-block; width:20%;" id="replayBtn" class="btn btn-success" onclick="replay()"><strong>Play again</strong></div>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
