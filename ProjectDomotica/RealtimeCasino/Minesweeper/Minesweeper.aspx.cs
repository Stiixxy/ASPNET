﻿using ProjectDomotica.RealtimeCasino.MinesweeperNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.RealtimeCasino {
	public partial class MinesweeperPage : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if(Session["UserID"] == null) {
				Response.Redirect("MasterPageTest.aspx");
			}
			if(Application["Minesweeper"] == null) {
				Application["Minesweeper"] = new Minesweeper();
			}
			Minesweeper game = (Minesweeper)Application["Minesweeper"];
			string hash = game.getAuthHash((int)Session["UserID"]);
			Response.Write("<script>var authHash = \"" + hash + "\";</script>");
		}
	}
}