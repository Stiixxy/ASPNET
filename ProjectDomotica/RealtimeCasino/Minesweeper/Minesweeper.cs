﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ProjectDomotica.RealtimeCasino.MinesweeperNamespace {
	public class Minesweeper {
		double increaseValue = 0.05;

		Dictionary<string, int> connections = new Dictionary<string, int>(); // connectionID, clientid
		Dictionary<string, int> authHashes = new Dictionary<string, int>(); // hash, clientid
		Dictionary<int, MinesweeperGame> games = new Dictionary<int, MinesweeperGame>(); //clientid, game

		IHubContext GameHub {
			get { return GlobalHost.ConnectionManager.GetHubContext<MinesweeperHub>(); }
		}

		public bool isConnected(int sessionID) {
			return connections.ContainsValue(sessionID);
		}

		public string getAuthHash(int userid) {
			string key = Global.GetUniqueKey(8);
			authHashes.Add(key, userid);
			return key;
		}

		public void connect(string hash, string connectionID) {
			int clientID; authHashes.TryGetValue(hash, out clientID);
			connections.Add(connectionID, clientID);
			if (games.ContainsKey(clientID)) {//A game is already running Send the game data;
				sendGameData(connectionID);
			} else {
				GameHub.Clients.Client(connectionID).nogame();
			}
			GameHub.Clients.Client(connectionID).setbalance(Global.getBalance(clientID));
		}

		public void createGame(string connectionid, double wager, int bombs) {
			int clientid; connections.TryGetValue(connectionid, out clientid);
			if (wager < Global.getBalance(clientid)) {
				MinesweeperGame game; if (!games.TryGetValue(clientid, out game)) {
					game = new MinesweeperGame(wager, increaseValue, bombs);
					games.Add(clientid, game);
					Global.addBalance(clientid, wager * -1);
					GameHub.Clients.Client(connectionid).setbalance(Global.getBalance(clientid));
				}
				GameHub.Clients.Client(connectionid).updategamedata(game.clickedTiles, game.wager, game.clickValue, game.returnValue);
			}
		}

		public void sendGameData(string connectionid) {
			int clientid; connections.TryGetValue(connectionid, out clientid);
			MinesweeperGame game; games.TryGetValue(clientid, out game);
			GameHub.Clients.Client(connectionid).updategamedata(game.clickedTiles, game.wager, game.clickValue, game.returnValue);
		}

		public void cashout(string connectionid) {
			int clientid; connections.TryGetValue(connectionid, out clientid);
			MinesweeperGame game; if (games.TryGetValue(clientid, out game)) {
				double profit = game.returnValue;
				Global.addBalance(clientid, profit);
				games.Remove(clientid);
				double newBalance = Global.getBalance(clientid);
				GameHub.Clients.Client(connectionid).endgame(profit, newBalance, game.bombs);
			}
		}

		public void click(string connectionid, int x, int y) {
			int clientid; connections.TryGetValue(connectionid, out clientid);
			MinesweeperGame game; if (games.TryGetValue(clientid, out game)) {
				bool succes = game.click(x, y);
				if (succes) {
					GameHub.Clients.Client(connectionid).updategamedata(game.clickedTiles, game.wager, game.clickValue, game.returnValue);
				} else {
					double newBalance = Global.getBalance(clientid);
					GameHub.Clients.Client(connectionid).endgame(0, newBalance, game.bombs);
					games.Remove(clientid);
				}
			}
		}

		public void Disconnect(string connectionid) {
			connections.Remove(connectionid);
		}

	}

	public class MinesweeperHub : Hub {
		static Minesweeper game;

		public override Task OnDisconnected() {
			game.Disconnect(Context.ConnectionId);
			return base.OnDisconnected();
		}

		public void auth(string authHash) {
			if (game == null) {
				game = (Minesweeper)HttpContext.Current.Application["Minesweeper"];
			}
			game.connect(authHash, Context.ConnectionId);
		}

		public void playgame(string input, string bombs) {
			try {
				double wager = double.Parse(input);
				int bombCount = int.Parse(bombs);
				game.createGame(Context.ConnectionId, wager, bombCount);
			} catch (Exception) {
				return;
			}
		}

		public void cashout() {
			game.cashout(Context.ConnectionId);
		}

		public void gridclick(int x, int y) {
			game.click(Context.ConnectionId, x, y);
		}
	}

	public class MinesweeperGame {
		public bool[,] bombs;
		public bool[,] clickedTiles;
		int size = 5;
		public double wager;
		public double returnValue;
		int clicks, bombCount;
		double increaseValue;
		public double clickValue {
			get {
				return returnValue * increaseValue *  bombCount;
				//return (Math.Round(wager * (Math.Pow((Math.Pow(increaseValue, bombCount)), clicks)), 2) / 10);
				//return Math.Pow((double)clicks / 5, 2) * wager / 10d * (double) bombCount;
			}
		}

		Random r;

		public MinesweeperGame(double wager, double increaseValue, int bombCount) {
			r = new Random();
			this.wager = wager;
			this.increaseValue = increaseValue;
			this.bombCount = bombCount;
			returnValue = wager;
			clicks = 1;
			generateBombs();
		}

		public void generateBombs() {
			bombs = new bool[size, size];
			clickedTiles = new bool[size, size];
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					bombs[x, y] = false;
					clickedTiles[x, y] = false;
				}
			}
			for (int i = 0; i < bombCount; i++) {
				int x;
				int y;
				do {
					x = r.Next(0, 5);
					y = r.Next(0, 5);
				} while (bombs[x, y]);
				bombs[x, y] = true;
			}
		}

		public bool click(int x, int y) {
			if (!clickedTiles[x, y]) {
				if (!bombs[x, y]) {// No boom
					returnValue += clickValue;
					clicks++;
					clickedTiles[x, y] = true;
					return true;
				} else { return false; }//Boom boom
			} else { return true; }
		}
	}
}