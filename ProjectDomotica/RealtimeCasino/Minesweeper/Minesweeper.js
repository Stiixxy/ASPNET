﻿$(function () {

	var chat = $.connection.minesweeperHub;

	chat.client.nogame = function () {
		showStartGame();
	};

	chat.client.setbalance = function (value) {
		setBalance(value);
		console.log("test: " + value);
	};

	chat.client.updategamedata = function (clickedTiles, wager, clickValue, returnValue) {
		clearGrid();
		setNextClick(clickValue);
		setCashout(returnValue);

		var grid = document.getElementById("grid");
		var gridChildren = grid.children;
		for (var x = 0; x < gridChildren.length; x++) {
			var divChildren = gridChildren[x].children;
			for (var y = 0; y < divChildren.length; y++) {
				if (clickedTiles[x][y]) {
					divChildren[y].style.setProperty("background-color", "green");
				}
			}
		}
		showGrid();
	};

	chat.client.endgame = function(profit, newBalance, bombs){
		setBalance(newBalance);
		var grid = document.getElementById("grid");
		var gridChildren = grid.children;
		for (var y = 0; y < gridChildren.length; y++) {
			var divChildren = gridChildren[y].children;
			for (var x = 0; x < divChildren.length; x++) {
				if (bombs[y][x]) {
					divChildren[x].style.setProperty("background-color", "red");
				}
			}
		}
		showEndGame();
	}

	$.connection.hub.start().done(function () {
		chat.server.auth(authHash);
	});
});

function clearGrid() {
	var grid = document.getElementById("grid");
	var gridChildren = grid.children;
	for (var i = 0; i < gridChildren.length; i++) {
		var divChildren = gridChildren[i].children;
		for (var j = 0; j < divChildren.length; j++) {
			console.log(divChildren[j].style.removeProperty("background-color"));
		}
	}
}

function showGrid() {
	var div = document.getElementById("startGame");
	div.style.setProperty("visibility", "hidden");
	div = document.getElementById("runningGame");
	div.style.setProperty("visibility", "visible")
	div = document.getElementById("endgameBtn");
	div.style.setProperty("visibility", "hidden")
}

function showStartGame() {
	var div = document.getElementById("startGame");
	div.style.setProperty("visibility", "visible");
	div = document.getElementById("runningGame");
	div.style.setProperty("visibility", "hidden")
	div = document.getElementById("endgameBtn");
	div.style.setProperty("visibility", "hidden")
}

function showEndGame() {
	var div = document.getElementById("endgameBtn");
	div.style.setProperty("visibility", "visible");
}

function hideEndGame() {
	var div = document.getElementById("endgameBtn");
	div.style.setProperty("visibility", "hidden");
}

function setTileGreen(x, y){
	var grid = document.getElementById("grid");
	var gridChildren = grid.children;
	var divChildren = gridChildren[x].children;
	divChildren[y].style.setProperty("background-color", "green");
}

function setTileRed(x, y) {
	var grid = document.getElementById("grid");
	var gridChildren = grid.children;
	var divChildren = gridChildren[x].children;
	divChildren[y].style.setProperty("background-color", "red");
}

function setNextClick(value) {
	value = Math.round(value * 100) / 100;
	var p = document.getElementById("nextClick")
	p.innerHTML = "Next Click: +" + value;
}

function setCashout(value) {
	value = Math.round(value * 100) / 100;
	var button = document.getElementById("cashoutBtn");
	button.innerHTML = "<strong>Cashout:</strong><br /> " + value + " cr";
}

function setBalance(value) {
	value = Math.round(value * 100) / 100;
	var label = document.getElementById("ContentPlaceHolder1_balanceHeader");
	label.innerHTML = "Balance: " + value;
}

function playGameClick() {
	var textbox = document.getElementById("ContentPlaceHolder1_txtWager");
	var wager = textbox.value;
	var textbox2 = document.getElementById("ContentPlaceHolder1_txtBombs")
	var bombs = textbox2.value;
	var chat = $.connection.minesweeperHub;
	chat.server.playgame(wager, bombs);
}

function cashout() {
	var chat = $.connection.minesweeperHub;
	chat.server.cashout();
	showEndGame();
}

function endGameClick() {
	showStartGame();
	hideEndGame();
}

function onGridClick(x, y) {
	var chat = $.connection.minesweeperHub;
	chat.server.gridclick(y, x);
}

function replay() {
	endGameClick();
	playGameClick();
}