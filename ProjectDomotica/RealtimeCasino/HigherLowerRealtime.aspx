﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMasterPage.Master" AutoEventWireup="true" CodeBehind="HigherLowerRealtime.aspx.cs" Inherits="ProjectDomotica.RealtimeCasino.HigherLowerRealtime" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<script src="/Scripts/jquery-1.6.4.min.js"></script>
	<script src="/Scripts/jquery.signalR-1.2.2.min.js"></script>
	<script src="/signalr/hubs"></script>
	<script src="/RealtimeCasino/HigherLower.js"></script>
	
	<div style="width: 50%; margin: 10% auto auto auto; border: 3px solid black; padding: 5px 5px 5px 5px; border-radius: 10px;">

		<h4 style="width: 100%; text-align: center; margin: 20px auto auto auto;">Higher lower</h4>
		<h5 id="balanceHeader" style="width: 100%; text-align: center; margin: 10px auto auto auto;" runat="server">Balance: </h5>

		<p style="width: 100%; text-align: center; margin: 30px auto auto auto" id="currentP" runat="server">Current: </p>
		<p style="width: 100%; text-align: center; margin: auto auto auto auto" id="previousP" runat="server"></p>
		<div style="margin: 30px auto auto auto; text-align: center;">
			<p>Wager:
				<asp:TextBox ID="txtWager" runat="server"></asp:TextBox></p>
		</div>
		<div style="margin: 10px auto 10px auto; text-align: center">
			<div id="btnHigher" class="btn btn-danger" style="width:30%; padding: 5px 10px 5px 10px; margin-right: 5%" onclick="higherClick()" >Higher</div>
			<div id="btnLower" class="btn btn-success" style="width: 30%; padding: 5px 10px 5px 10px" onclick="lowerClick()" >Lower</div>
		</div>

		<div class="alert" role="alert" id="divResponse" runat="server" style="width: 100%; margin: 5% auto auto auto;"></div>

	</div>
</asp:Content>
