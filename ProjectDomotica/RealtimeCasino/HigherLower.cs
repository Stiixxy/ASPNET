﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ProjectDomotica.RealtimeCasino {
	public class HigherLowerGame {

		double winPercentage = 0.25;

		Dictionary<string, int> connections = new Dictionary<string, int>(); //Key = connectionid; value = clientid;
		Dictionary<string, int> authHashes = new Dictionary<string, int>(); //Key = secretHash, value = clientid;
		Dictionary<int, int> clientNumbers = new Dictionary<int, int>(); //Key = clientid; value = clients current number;
		Random r = new Random();

		public bool isConnected(int sessionID) {
			return connections.ContainsValue(sessionID);
		}

		public string getAuthHash(int userid) {
			string key = Global.GetUniqueKey(8);
			authHashes.Add(key, userid);
			return key;
		}

		public int auth(string secretHash, string ConnectionId, out double balance) {
			if (authHashes.ContainsKey(secretHash)) {
				int clientid;
				authHashes.TryGetValue(secretHash, out clientid);
				connections.Add(ConnectionId, clientid);

				authHashes.Remove(secretHash);
				System.Diagnostics.Debug.Write("Accepted client: " + clientid);
				int number = 0;
				if (clientNumbers.ContainsKey(clientid)) {
					clientNumbers.TryGetValue(clientid, out number);
				} else {
					number = r.Next(0, 100);
					clientNumbers.Add(clientid, number);
				}
				balance = Global.getBalance(clientid);
				return number;
			} else {
				balance = 0;
				return -1;
			}
		}

		public void higher(string connectionid, double wager) {
			int currentNumber;
			int clientid;
			string connectionID = connectionid;
			connections.TryGetValue(connectionid, out clientid);
			clientNumbers.TryGetValue(clientid, out currentNumber);
			if (Global.getBalance(clientid) < wager) {
				return;
			}
			//We got all data, calculate result
			int nextNumber = r.Next(0, 100);
			double profit = 0;
			if (nextNumber > currentNumber) {
				profit = wager * winPercentage;
			} else {
				profit = wager * -1;
			}
			//Save result and send to client
			Global.addBalance(clientid, profit);
			double newBalance = Global.getBalance(clientid);
			clientNumbers.Remove(clientid);
			clientNumbers.Add(clientid, nextNumber);
			IHubContext context = GlobalHost.ConnectionManager.GetHubContext<HigherLower>();
			context.Clients.Client(connectionid).update(nextNumber, profit, newBalance);
		}

		public void lower(string connectionid, double wager) {
			int currentNumber;
			int clientid;
			connections.TryGetValue(connectionid, out clientid);
			clientNumbers.TryGetValue(clientid, out currentNumber);
			if (Global.getBalance(clientid) < wager) {
				return;
			}
			//We got all data, calculate result
			int nextNumber = r.Next(0, 100);
			double profit = 0;
			if (nextNumber < currentNumber) {
				profit = wager * winPercentage;
			} else {
				profit = wager * -1;
			}
			//Save result and send to client
			Global.addBalance(clientid, profit);
			double newBalance = Global.getBalance(clientid);
			clientNumbers.Remove(clientid);
			clientNumbers.Add(clientid, nextNumber);
			IHubContext context = GlobalHost.ConnectionManager.GetHubContext<HigherLower>();
			context.Clients.Client(connectionid).update(nextNumber, profit, newBalance);
		}

		public void getData(string connectionid) {
			int currentNumber;
			int clientid;
			connections.TryGetValue(connectionid, out clientid);
			clientNumbers.TryGetValue(clientid, out currentNumber);

			IHubContext context = GlobalHost.ConnectionManager.GetHubContext<HigherLower>();
			context.Clients.Client(connectionid).begin(currentNumber);
		}
	}

	public class HigherLower : Hub {
		//Only manages connection and functions
		//Context.ConnectionId;

		HigherLowerGame game;

		public void auth(string secretHash) {
			if (game == null) {
				game = (HigherLowerGame)HttpContext.Current.Application["HigherLower"];
			}
			double balance;
			int accepted = game.auth(secretHash, Context.ConnectionId, out balance);
			if (accepted >= 0) { Clients.Client(Context.ConnectionId).accepted(accepted, balance); }
		}

		public void getdata() {
			if (game == null) {
				game = (HigherLowerGame)HttpContext.Current.Application["HigherLower"];
			}
			game.getData(Context.ConnectionId);
		}

		public void higher(string input) {
			double wager = double.Parse(input);
			if (game == null) {
				game = (HigherLowerGame)HttpContext.Current.Application["HigherLower"];
			}
			game.higher(Context.ConnectionId, wager);
		}

		public void lower(string input) {
			double wager = double.Parse(input);
			if (game == null) {
				game = (HigherLowerGame)HttpContext.Current.Application["HigherLower"];
			}
			game.lower(Context.ConnectionId, wager);
		}
	}
}