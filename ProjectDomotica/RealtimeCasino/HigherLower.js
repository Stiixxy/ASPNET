﻿$(function () {
	//Declare reference to the hub
	var chat = $.connection.higherLower;

	//Add function accepted that will be called by the hub
	chat.client.accepted = function (number, balance) {
		var current = document.getElementById("ContentPlaceHolder1_currentP");
		current.textContent = "Current: " + number;
		var divBalance = document.getElementById("ContentPlaceHolder1_balanceHeader");
		divBalance.innerHTML = "Balance: " + balance;
		console.log("Our connection was accepted");
	};

	//Add function update that will be called by the hub
	chat.client.update = function (number, result, newbalance) {
		var current = document.getElementById("ContentPlaceHolder1_currentP");
		var previous = document.getElementById("ContentPlaceHolder1_previousP");
		previous.textContent = "Previous: " + current.textContent.substring(9);
		current.textContent = "Current: " + number;

		var divResponse = document.getElementById("ContentPlaceHolder1_divResponse");
		if (result < 0) {
			divResponse.classList = "alert alert-danger";
			divResponse.innerHTML = "<strong>Too bad</strong> You lost " + result + " credits!";
		} else {
			divResponse.classList = "alert alert-success";
			divResponse.innerHTML = "<strong>Congratulations</strong> You won " + result + " credits!";
		}
		var divBalance = document.getElementById("ContentPlaceHolder1_balanceHeader");
		divBalance.innerHTML = "Balance: " + newbalance;
	}

	chat.client.begin = function (number) {
		var current = document.getElementById("ContentPlaceHolder1_currentP");
		current.textContent = "Current: " + number;
	}

	//Open connection
	$.connection.hub.start().done(function () {
		var chat = $.connection.higherLower;
		if (!connected) {
			chat.server.auth(authHash);
		} else {
			chat.server.getdata();
		}
	});
});

function higherClick() {
	var button = document.getElementById("btnHigher");
	if (button.getAttribute("disabled") == null) {
		var chat = $.connection.higherLower;
		var textbox = document.getElementById("ContentPlaceHolder1_txtWager");
		var button = document.getElementById("btnHigher");
		button.setAttribute("disabled", "disabled");
		var button = document.getElementById("btnLower");
		button.setAttribute("disabled", "disabled");
		setTimeout(enableHigher, 200);
		//console.log(textbox.value);
		chat.server.higher(textbox.value);
	}
};

function lowerClick() {
	var button = document.getElementById("btnLower");
	if (button.getAttribute("disabled") == null) {
		var chat = $.connection.higherLower;
		var textbox = document.getElementById("ContentPlaceHolder1_txtWager");
		var button = document.getElementById("btnLower");
		button.setAttribute("disabled", "disabled");
		var button = document.getElementById("btnHigher");
		button.setAttribute("disabled", "disabled");
		setTimeout(enableLower, 200);
		//console.log(textbox.value);
		chat.server.lower(textbox.value);
	}
};


function enableHigher() {
	var button = document.getElementById("btnHigher");
	button.removeAttribute("disabled")
	enableLower();
}

function enableLower() {
	var button = document.getElementById("btnLower");
	button.removeAttribute("disabled")
	enableHigher();
}