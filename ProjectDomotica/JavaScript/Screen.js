﻿function setSizeToHiddenObj() {
    document.getElementById("hiddenValue").style.width = window.innerWidth + "px";
    document.getElementById("hiddenValue").style.height = window.innerHeight + "px";
    var width = window.innerWidth;
    var height = window.innerHeight;

    //renderGrid(width, height);
}

function renderGrid(width, height) {

    var HorizontalBlocks = 10;
    var VerticalBlocks = 4;
    var _width = 100;
    var _height = 100;
    var _distance = 10;
    var top = 0;
    var left = 0;
    var totalWidth = 0;
    var totalHeight = 0;

    for (var i = 0; i < VerticalBlocks; i++) {
        for (var x = 0; x < HorizontalBlocks; x++) {
            var Div = document.createElement("DIV");

            document.getElementById("GridContainer").appendChild(Div)

            Div.ID = "Box_" + x + "_" + i;
            Div.style.width = _width + "px";
            Div.style.height = _height + "px";
            Div.style.top = top + "px";
            Div.style.left = left + "px";
            Div.className = "block";
            Div.setAttribute("onclick", "div_click(this.id)");
            Div.setAttribute("onmouseover", "div_mouseOver(this.id)");
            Div.setAttribute("onmouseout", "div_mouseOut(this.id)");

            left = left + _width + _distance;
            console.log(left.toString());
        }

        totalWidth = left;

        left = 0;
        top = top + _height + _distance;
    }

    totalHeight = top;
    
    document.getElementById("GridContainer").style.marginLeft = "-" + (totalWidth / 2) + "px";
    document.getElementById("GridContainer").style.marginTop = "-" + (totalHeight / 2) + "px";
}