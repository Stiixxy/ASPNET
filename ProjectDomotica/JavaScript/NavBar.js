﻿function toggleLogin() {
	var loginDiv = document.getElementById("loginDiv");
	if (loginDiv.style.visibility === 'hidden') {
		loginDiv.style.visibility = 'visible';
		document.getElementById("registerDiv").style.visibility = 'hidden';
	} else {
		loginDiv.style.visibility = 'hidden';
	}
}

function toggleRegister() {
	var registerDiv = document.getElementById("registerDiv");
	if (registerDiv.style.visibility === 'hidden') {
		registerDiv.style.visibility = 'visible';
		document.getElementById("loginDiv").style.visibility = 'hidden';
	} else {
		registerDiv.style.visibility = 'hidden';
	}
}