﻿function div_click(clicked_id) {
    if (document.getElementById("GridContainer").classList.contains("editMode")) {
        if (document.getElementById(clicked_id).classList.contains("block")) {
            if (document.getElementById(clicked_id).classList.contains("Selected")) {
                document.getElementById(clicked_id).classList.remove("Selected");

            } else {
                var elements = document.getElementsByClassName("Selected");

                if (elements.length > 0) {
                    elements[0].classList.remove("Selected");
                }
                document.getElementById(clicked_id).classList.add("Selected");
                document.getElementById(clicked_id).classList.toggle("Hoover");
            }
        }
    }
}

function div_mouseOver(hoover_id) {
    if (document.getElementById("GridContainer").classList.contains("editMode")) {
        if (document.getElementById(hoover_id).classList.contains("block")) {
            if (!(document.getElementById(hoover_id).classList.contains("Selected"))) {
                document.getElementById(hoover_id).classList.add("Hoover");
            }
        }
    }
}

function div_mouseOut(hoover_id) {
    if (document.getElementById("GridContainer").classList.contains("editMode")) {
        if (document.getElementById(hoover_id).classList.contains("block")) {
            var elements = document.getElementsByClassName("Hoover");

            if (elements.length > 0) {
                elements[0].classList.remove("Hoover");
            }
        }
    }
}