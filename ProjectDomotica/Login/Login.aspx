﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ProjectDomotica.Login.Loginn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<asp:Button ID="BtnRegister" runat="server" Text="Register" style="float:right" OnClick="BtnRegister_Click" />
			<h2 style="margin-left:100px">Login</h2>
			<table>
				<tr>
					<th><p>Username:</p></th>
					<th><asp:TextBox ID="txtUsername" runat="server"></asp:TextBox></th>
				</tr>
				<tr>
					<th><p>Password:</p></th>
					<th><asp:TextBox ID="txtPassword" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox></th>
				</tr>
			</table>

			<div id="divResponse" runat="server"></div>
			<asp:Button ID="btnSumbit" runat="server" Text="Login" style="margin-left:80px; margin-top:20px; padding-left:20px; padding-right:20px" OnClick="btnSumbit_Click"/>
		</div>
	</form>
</body>
</html>
