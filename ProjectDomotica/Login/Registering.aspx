﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registering.aspx.cs" Inherits="ProjectDomotica.Login.Registering" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<asp:Button ID="BtnLogin" runat="server" Text="Login" style="float:right" OnClick="BtnLogin_Click"/>
    	<h2 style="margin-left:80px">Register</h2>
		<div>
			<table>
				<tr>
					<th><p>Username:</p></th>
					<th><asp:TextBox ID="txtUsername" runat="server"></asp:TextBox></th>
				</tr>
				<tr>
					<th><p>E-Mail:</p></th>
					<th><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></th>
				</tr>
				<tr>
					<th><p>Password:</p></th>
					<th><asp:TextBox ID="txtPassword" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox></th>
				</tr>
				<tr>
					<th><p>Repeat Password:</p></th>
					<th><asp:TextBox ID="txtPasswordRepeat" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox></th>
				</tr>
			</table>

			<div id="divResponse" runat="server"></div>
			<asp:Button ID="btnSumbit" runat="server" Text="Register" style="margin-left:80px; margin-top:20px; padding-left:20px; padding-right:20px" OnClick="btnSumbit_Click"/>
		</div>
		
    </div>
    </form>
</body>
</html>
