﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.Login {
	public partial class Loginn : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
		}

		protected void btnSumbit_Click(object sender, EventArgs e) {
			string password = txtPassword.Text;
			string username = txtUsername.Text;
			if (username.Equals("")) {
				divResponse.InnerHtml = "Please enter an username";
			} else if (password.Equals("")) {
				divResponse.InnerHtml = "Please enter a password!";
			} else {
				password = Global.getMD5(password);
				MySqlCommand command = new MySqlCommand("SELECT userid, username FROM users WHERE username = ? AND password = ?;");
				command.Parameters.Add(username);
				command.Parameters.Add(password);
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(command);
				if (reader.HasRows) {
					reader.Read();

					int userID = reader.GetInt32(0);
					Session["UserID"] = userID;
					divResponse.InnerHtml = "Logged in succesfully. Welcome " + reader.GetString(1);
					reader.Close();

					Response.Redirect("~/MasterPageTest.aspx");


				} else {
					divResponse.InnerHtml = "Invalid credentials!";
					reader.Close();
				}

			}
		}

		protected void BtnRegister_Click(object sender, EventArgs e) {
			Server.Transfer("Registering.aspx");
		}
	}
}