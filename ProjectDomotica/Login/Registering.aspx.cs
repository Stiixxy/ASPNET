﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.Login {
	public partial class Registering : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {

		}

		protected void btnSumbit_Click(object sender, EventArgs e) {
			string email = txtEmail.Text;
			string username = txtUsername.Text;
			string password = txtPassword.Text;
			string confirmPassword = txtPasswordRepeat.Text;

			if (!password.Equals(confirmPassword)) {
				divResponse.InnerHtml = "The passwords do not match";
			} else {
				MySqlCommand command = new MySqlCommand("SELECT email FROM users WHERE email=?;");
				command.Parameters.Add(email);
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(command);
				if (reader.HasRows) {
					divResponse.InnerHtml = "Email is already in use";
					reader.Close();
				} else {
					reader.Close();
					command = new MySqlCommand("SELECT username FROM users WHERE username =?");
					command.Parameters.Add(username);
					reader = Global.SqlConnection.executeSQLCommand(command);
					if (reader.HasRows) {
						divResponse.InnerHtml = "Username is already in use";
						reader.Close();
					} else {
						//We can register
						reader.Close();
						if (!Global.checkEmail(email)) {
							divResponse.InnerHtml = "Please enter a valid email!";
						} else {
							string encryptedPass = Global.getMD5(password);
							command = new MySqlCommand("INSERT INTO users (email, username, password) VALUES (?,?,?)");
							command.Parameters.Add(email);
							command.Parameters.Add(username);
							command.Parameters.Add(encryptedPass);
							reader = Global.SqlConnection.executeSQLCommand(command);
							if (reader.RecordsAffected > 0) {
								divResponse.InnerHtml = "Registration succesful";
							} else {
								divResponse.InnerHtml = "Somthing went wrong!";
							}
							reader.Close();
						}
					}
				}
			}
		}

		protected void BtnLogin_Click(object sender, EventArgs e) {
			Server.Transfer("Login.aspx");
		}
	}
}