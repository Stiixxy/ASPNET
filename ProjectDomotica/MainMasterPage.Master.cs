﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica {
	public partial class MainMasterPage : System.Web.UI.MasterPage {
		protected void Page_Load(object sender, EventArgs e) {
			form3.Controls.AddAt(0, Global.getMenu());

			if (!IsPostBack) {
				if (Request.QueryString["action"] != null && Request.QueryString["action"].Equals("login")) {
					if (Request.QueryString["error"] != null) {
						loginDiv.Style["visibility"] = "visible";

						switch (Request.QueryString["error"]) {
							case "0":
								divLoginResponse.InnerHtml = "Please enter an username";
								break;
							case "1":
								divLoginResponse.InnerHtml = "Please enter a password";
								if (Request.QueryString["username"] != null) txtUsername.Text = txtUsername.Text = Request.QueryString["username"];
								break;
							case "2":
								divLoginResponse.InnerHtml = "Invalid credentials";
								if (Request.QueryString["username"] != null) txtUsername.Text = txtUsername.Text = Request.QueryString["username"];
								break;
						}

					}
				} else if (Request.QueryString["action"] != null && Request.QueryString["action"].Equals("register")) {
					//5 cases
					if(Request.QueryString["error"] != null) {
						registerDiv.Style["visibility"] = "visible";

						switch (Request.QueryString["error"]) {
							case "0":
								divRegisterResponse.InnerHtml = "Please fill out all forms";
								break;
							case "1":
								divRegisterResponse.InnerHtml = "Passwords do not match";
								if (Request.QueryString["username"] != null) txtRegisterUsername.Text = Request.QueryString["username"];
								if (Request.QueryString["email"] != null) txtRegisterEmail.Text = Request.QueryString["email"];
								break;
							case "2":
								divRegisterResponse.InnerHtml = "email is already in use";
								if (Request.QueryString["username"] != null) txtRegisterUsername.Text = Request.QueryString["username"];
								if (Request.QueryString["email"] != null) txtRegisterEmail.Text = Request.QueryString["email"];
								break;
							case "3":
								divRegisterResponse.InnerHtml = "username is already in use";
								if (Request.QueryString["username"] != null) txtRegisterUsername.Text = Request.QueryString["username"];
								if (Request.QueryString["email"] != null) txtRegisterEmail.Text = Request.QueryString["email"];
								break;
							case "4":
								divRegisterResponse.InnerHtml = "please enter a valid email adress";
								if (Request.QueryString["username"] != null) txtRegisterUsername.Text = Request.QueryString["username"];
								if (Request.QueryString["email"] != null) txtRegisterEmail.Text = Request.QueryString["email"];
								break;
						}
					}
				}
			}
		}

		protected void btnSumbit_Click(object sender, EventArgs e) {
			string password = txtPassword.Text;
			string username = txtUsername.Text;
			if (username.Equals("")) {
				divLoginResponse.InnerHtml = "Please enter an username";
				Response.Redirect(SiteMap.CurrentNode.Url + "?action=login&error=0");
			} else if (password.Equals("")) {
				divLoginResponse.InnerHtml = "Please enter a password!";
				Response.Redirect(SiteMap.CurrentNode.Url + "?action=login&error=1&username=" + username);
			} else {
				password = Global.getMD5(password);
				MySqlCommand command = new MySqlCommand("SELECT userid, username FROM users WHERE username = ? AND password = ?;");
				command.Parameters.Add(new MySqlParameter("username", username));
				command.Parameters.Add(new MySqlParameter("password", password));
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(command);
				if (reader.HasRows) {
					reader.Read();

					int userID = reader.GetInt32(0);
					Session["UserID"] = userID;
					reader.Close();
					Response.Redirect(SiteMap.CurrentNode.Url);
				} else {
					reader.Close();
					Response.Redirect(SiteMap.CurrentNode.Url + "?action=login&error=2&username=" + username);
				}
			}
		}

		protected void btnRegister_Click(object sender, EventArgs e) {
			string email = txtRegisterEmail.Text;
			string username = txtRegisterUsername.Text;
			string password = txtRegisterPassword.Text;
			string confirmPassword = txtRegisterPasswordRepeat.Text;

			if(username.Equals("") || username.Equals("") || password.Equals("")) {
				Response.Redirect(SiteMap.CurrentNode.Url + "?action=register&error=0");
			} else if (!password.Equals(confirmPassword)) {
				Response.Redirect(SiteMap.CurrentNode.Url + "?action=register&error=1&username=" + username + "&email=" + email);
			} else {
				MySqlCommand command = new MySqlCommand("SELECT email FROM users WHERE email=?;");
				command.Parameters.Add(email);
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(command);
				if (reader.HasRows) {
					reader.Close();
					Response.Redirect(SiteMap.CurrentNode.Url + "?action=register&error=2&username=" + username + "&email=" + email);
				} else {
					reader.Close();
					command = new MySqlCommand("SELECT username FROM users WHERE username =?");
					command.Parameters.Add(username);
					reader = Global.SqlConnection.executeSQLCommand(command);
					if (reader.HasRows) {
						reader.Close();
						Response.Redirect(SiteMap.CurrentNode.Url + SiteMap.CurrentNode.Url + "?action=register&error=3&username=" + username + "&email=" + email);
					} else {
						//We can register
						reader.Close();
						if (!Global.checkEmail(email)) {
							Response.Redirect(SiteMap.CurrentNode.Url + "?action=register&error=4&username=" + username + "&email=" + email);
						} else {
							string encryptedPass = Global.getMD5(password);
							command = new MySqlCommand("INSERT INTO users (email, username, password) VALUES (?,?,?)");
							command.Parameters.Add(email);
							command.Parameters.Add(username);
							command.Parameters.Add(encryptedPass);
							reader = Global.SqlConnection.executeSQLCommand(command);
							if (reader.RecordsAffected > 0) {
								reader.Close();
								command = new MySqlCommand("SELECT userid FROM users WHERE username = ? AND password = ?;");
								command.Parameters.Add(username);
								command.Parameters.Add(encryptedPass);
								reader = Global.SqlConnection.executeSQLCommand(command);
								if (reader.HasRows) {
									reader.Read();
									int userid = reader.GetInt32(0);
									reader.Close();
									Session["UserID"] = userid;
								}
								reader.Close();
								
								Response.Redirect(SiteMap.CurrentNode.Url + "?");
							} else {
								divRegisterResponse.InnerHtml = "Somthing went wrong!";
								Response.Redirect(SiteMap.CurrentNode.Url);
								reader.Close();
							}
						}
					}
				}
			}
		}
	}
}