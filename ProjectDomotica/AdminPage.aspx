﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMasterPage.Master" AutoEventWireup="true" CodeBehind="AdminPage.aspx.cs" Inherits="ProjectDomotica.AdminPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<script type="text/javascript">
		function moveItem(type) {
			var items;
			var otherItems;
			if (type == 0) {
				items = document.getElementById("ContentPlaceHolder1_AdminBox");
				otherItems = document.getElementById("ContentPlaceHolder1_NonAdminBox");
			} else {
				otherItems = document.getElementById("ContentPlaceHolder1_AdminBox");
				items = document.getElementById("ContentPlaceHolder1_NonAdminBox");
			}
			var selected = items.selectedOptions[0];
			otherItems.appendChild(selected);
			selected.setAttribute("changed", "true");
		};
	</script>

	<div id="main" style="width: 50%">
		<div id="panelDiv" style="display: inline">
			<asp:ListBox ID="AdminBox" Style="height: 80%; width: 44%; margin: 0 5% 0 0;" runat="server"></asp:ListBox>
			<asp:ListBox ID="NonAdminBox" Style="height: 80%; width: 44%; margin: 0 0 0 5%;" runat="server"></asp:ListBox>
		</div>
		<div id="buttonDiv" style="text-align: center">
			<asp:Button ID="BtnToNonAdmin" AutoPostBack="True" runat="server" Text=">" class="btn btn-default" OnClick="BtnToNonAdmin_Click" />
			<asp:Button ID="BtnToAdmin" AutoPostBack="True" runat="server" Text="<" class="btn btn-default" OnClick="BtnToAdmin_Click" />
		</div>
	</div>

	<div style="margin-top: 5%; width: 50%;">
		<p>
			<asp:ListBox ID="users" Style="height: 100%; width: 50%" runat="server"></asp:ListBox>
			<asp:TextBox ID="newBalance" runat="server" Style="vertical-align: middle"></asp:TextBox>
			<asp:Button ID="applyBalance" runat="server" Text="Set balance" style="vertical-align: middle" OnClick="applyBalance_Click"/>
		</p>
	</div>

</asp:Content>
