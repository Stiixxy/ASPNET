﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.ComponentModel.DataAnnotations;
using System.Security.Policy;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Routing;

namespace ProjectDomotica {
	public class Global : System.Web.HttpApplication {
		private static DaHausControl _DaHouse;
		public static DaHausControl DaHaus {
			get {
				if (_DaHouse == null) {
					_DaHouse = new DaHausControl();
				}
				if (!_DaHouse.getConnected()) {
					_DaHouse.connect();
				}
				return _DaHouse;
			}
		}
		private static DatabaseConnection _SqlConnection;
		public static DatabaseConnection SqlConnection {
			get {
				if (_SqlConnection == null) {
					_SqlConnection = new DatabaseConnection();
				}
				return _SqlConnection;
			}
		}

		protected void Application_Start(object sender, EventArgs e) {
			RouteTable.Routes.MapHubs();
			SqlConnection.openConnection();
			DaHaus.connect();
		}

		protected void Session_Start(object sender, EventArgs e) {
			
		}

		protected void Application_BeginRequest(object sender, EventArgs e) {

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e) {

		}

		protected void Application_Error(object sender, EventArgs e) {

		}

		protected void Session_End(object sender, EventArgs e) {
			
		}

		protected void Application_End(object sender, EventArgs e) {
		}

		public static string GetUniqueKey(int maxSize) {
			char[] chars = new char[62];
			chars =
			"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
			byte[] data = new byte[1];
			using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider()) {
				crypto.GetNonZeroBytes(data);
				data = new byte[maxSize];
				crypto.GetNonZeroBytes(data);
			}
			StringBuilder result = new StringBuilder(maxSize);
			foreach (byte b in data) {
				result.Append(chars[b % (chars.Length)]);
			}
			return result.ToString();
		}

		public static DaHausControl getDaHaus() {
			return DaHaus;
		}

		public static DatabaseConnection getSqlConnection() {
			return SqlConnection;
		}

		public static bool isAdmin(int userID) {
			bool response = false;
			MySqlDataReader reader = SqlConnection.executeSQLCommand(new MySqlCommand("SELECT isAdmin FROM permissions WHERE userid = "+ userID));
			if (reader.HasRows) {
				reader.Read();
				if(!reader.IsDBNull(0) && reader.GetBoolean(0)) {
					response = true;
				}
			}
			reader.Close();
			return response;
		}

		public static bool checkEmail(string email) {
			string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
			+ @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
			+ @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

			return (new Regex(validEmailPattern, RegexOptions.IgnoreCase).IsMatch(email));
		}

		public static string getMD5(string input) {
			byte[] bytes = Encoding.ASCII.GetBytes(input);
			MD5 md5Hash = MD5.Create();
			md5Hash.ComputeHash(bytes);
			bytes = md5Hash.Hash;
			StringBuilder strBuilder = new StringBuilder();
			for (int i = 0; i < bytes.Length; i++) {
				strBuilder.Append(bytes[i].ToString("x2"));
			}
			return strBuilder.ToString();
		}

		public static string getUsername(int userID) {
			MySqlCommand command = new MySqlCommand("SELECT username FROM users WHERE userid=?;");
			command.Parameters.Add(new MySqlParameter("userid", userID));
			MySqlDataReader reader = SqlConnection.executeSQLCommand(command);
			reader.Read();
			string username = reader.GetString(0);
			reader.Close();
			return username;
		}

		public static double addBalance(int userID, double balance) {
			//UPDATE userdata SET balance = balance - 10 WHERE userid = 8;
			MySqlCommand command = new MySqlCommand("UPDATE userdata SET balance = balance + ? WHERE userid = ?;");
			command.Parameters.Add(new MySqlParameter("balance", balance));
			command.Parameters.Add(new MySqlParameter("userid", userID));
			MySqlDataReader reader = SqlConnection.executeSQLCommand(command);
			reader.Close();
			double newBalance = getBalance(userID);
			return newBalance;
		}

		public static double getBalance(int userID) {
			MySqlCommand command = new MySqlCommand("SELECT balance FROM userdata WHERE userid =?;");
			command.Parameters.Add(new MySqlParameter("userid", userID));
			MySqlDataReader reader = SqlConnection.executeSQLCommand(command);
			double balance = 0;
			if (reader.HasRows) {
				reader.Read();
				balance = reader.GetDouble(0);
				reader.Close();
			}else {
				reader.Close();
				command = new MySqlCommand("INSERT INTO userdata (userid, balance) VALUES (?, 0);");
				command.Parameters.Add(userID);
				reader = SqlConnection.executeSQLCommand(command);
				balance = 0;
				reader.Close();
			}
			return Math.Round(balance,2);
		}

		public static string replaceString(string input) {
			if (HttpContext.Current.Session["UserID"] != null) {
				input = input.Replace("{username}", getUsername((int)HttpContext.Current.Session["UserID"]));
				input = input.Replace("{balance}", getBalance((int)HttpContext.Current.Session["UserID"]).ToString());
			}

			return input;
		}

		protected static void logout(object sender, EventArgs e) {
			HttpContext.Current.Session.Abandon();
			HttpContext.Current.Response.Redirect(SiteMap.CurrentNode.Url);
		}

		public static void addfunction(Button btn, string function) {
			switch (function) {
				case "logout":
					btn.Click += new EventHandler(logout);
					break;

			}
		}

		public static HtmlGenericControl getMenu() {
			HtmlGenericControl menuDiv = new HtmlGenericControl("div");
			HtmlGenericControl menuUL = new HtmlGenericControl("ul");
			menuUL.Attributes["class"] = "nav nav-tabs";

			//foreach loop trough nodes
			SiteMapNode rootNode = SiteMap.RootNode.Clone();
			foreach (SiteMapNode MainNode in rootNode.ChildNodes) {
				if (MainNode["loggedin"] != null && MainNode["loggedin"].Equals("1")) {
					if (HttpContext.Current.Session["UserID"] == null) {
						continue;
					}
				}else if (MainNode["loggedin"] != null && MainNode["loggedin"].Equals("2")) {
					if(HttpContext.Current.Session["UserID"] != null){continue;}
				}else if(MainNode["loggedin"] != null && MainNode["loggedin"].Equals("3")) {
					if(HttpContext.Current.Session["UserID"] != null && !isAdmin((int)HttpContext.Current.Session["UserID"])){ continue; }
				}
				HtmlGenericControl menuLi = new HtmlGenericControl("li");
				if (MainNode == SiteMap.CurrentNode) {
					menuLi.Attributes["class"] = "active";
				}
				if (MainNode["position"] != null && MainNode["position"].Equals("right")) {
					menuLi.Attributes["style"] = "float:right";
				}
				if(MainNode["id"] != null) {
					menuLi.ID = MainNode["id"];
				}
				if(MainNode["onclick"] != null) {
					menuLi.Attributes.Add("onclick", MainNode["onclick"]);
				}
				if(MainNode["aspcontrol"] != null) {
					Button btn = new Button();
					btn.Text = "test";
					addfunction(btn, MainNode["aspcontrol"]);
					menuLi.Controls.Add(btn);
				}
				if (MainNode.HasChildNodes) {
					menuLi.Attributes.Add("class", "dropdown");
					menuLi.InnerHtml = "<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">" + replaceString(MainNode.Title) + " <span class=\"caret\"></span></a>";

					HtmlGenericControl dropdownUL = new HtmlGenericControl("ul");
					dropdownUL.Attributes.Add("class", "dropdown-menu");

					foreach(SiteMapNode dropdownNode in MainNode.ChildNodes) {
						HtmlGenericControl dropdownLi = new HtmlGenericControl("li");

						if(dropdownNode["loggedin"] != null && dropdownNode["loggedin"] == "3") {
							if (HttpContext.Current.Session["UserID"] != null && !isAdmin((int)HttpContext.Current.Session["UserID"])){ continue; }
						}
						if (dropdownNode == SiteMap.CurrentNode) {
							dropdownLi.Attributes["class"] = "active";
						}
						if (dropdownNode["id"] != null) {
							dropdownLi.ID = dropdownNode["id"];
						}
						if (dropdownNode["onclick"] != null) {
							dropdownLi.Attributes.Add("onclick", dropdownNode["onclick"]);
						}
						if (dropdownNode["aspcontrol"] != null) {
							Button btn = new Button();
							btn.Text = replaceString(dropdownNode.Title);
							btn.Attributes["type"] = "submit";
							btn.CssClass = "btn btn-default";
							addfunction(btn, dropdownNode["aspcontrol"]);
							dropdownLi.Controls.Add(btn);
						} else if (dropdownNode.Url.Equals("")) {
							dropdownLi.InnerHtml = "<a role=\"button\">" + replaceString(dropdownNode.Title) + "</a>";
						} else {
							dropdownLi.InnerHtml = "<a href=\"" + dropdownNode.Url + "\" role=\"button\">" + replaceString(dropdownNode.Title) + "</a>";
						}
						
												
						dropdownUL.Controls.Add(dropdownLi);
					}

					menuLi.Controls.Add(dropdownUL);

				} else {
					if (MainNode.Url.Equals("")) {
						menuLi.InnerHtml = "<a role=\"button\">" + replaceString(MainNode.Title) + "</a>";
					} else {
						menuLi.InnerHtml = "<a href=\"" + MainNode.Url + "\" role=\"button\">" + replaceString(MainNode.Title) + "</a>";
					}
				}

				menuUL.Controls.Add(menuLi);

			}

			menuDiv.Controls.Add(menuUL);
			return menuDiv;
		}
	}
}