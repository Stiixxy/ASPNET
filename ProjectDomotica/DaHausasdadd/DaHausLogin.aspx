﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DaHausLogin.aspx.cs" Inherits="ProjectDomotica.DaHausLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    	<asp:Label ID="Label1" runat="server" Text="Enter UserID       "></asp:Label>
		<asp:TextBox ID="TxtUserID" runat="server" Height="16px" MaxLength="3" Width="34px">0</asp:TextBox>
    </div>
	<div>
		<asp:Button ID="BtnLogin" runat="server" OnClick="BtnLogin_Click" Text="Login" /></asp:Button>
	</div>
    </form>
</body>
</html>
