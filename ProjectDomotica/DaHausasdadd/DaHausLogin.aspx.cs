﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica {
	public partial class DaHausLogin : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {

		}

		protected void BtnLogin_Click(object sender, EventArgs e) {
			HttpCookie cookie = Request.Cookies["SessionData"];
			if(cookie == null) {
				cookie = new HttpCookie("SessionData");
			}

			cookie["UserID"] = TxtUserID.Text;
			cookie.Expires = DateTime.Now.AddDays(2);
			Response.Cookies.Add(cookie);
			Server.Transfer("DaHausControlpage.aspx");
		}
	}
}