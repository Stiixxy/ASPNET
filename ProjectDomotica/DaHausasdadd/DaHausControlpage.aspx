﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DaHausControlpage.aspx.cs" Inherits="ProjectDomotica.DaHaus.DaHausControlpage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="refresh" content="5" />
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<asp:Label ID="LblWelcomeMessage" runat="server" Text="-"></asp:Label>
		</div>
		<div id="ButtonDiv" runat="server">
			<h3>Lights</h3>
			<div id="LightsDiv" runat="server">
				<asp:Button ID="BtnLight0" Text="Light 0 : -" runat="server" OnClick="BtnLight0_Click" />
				<asp:Button ID="BtnLight1" Text="Light 1 : -" runat="server" OnClick="BtnLight1_Click" />
				<asp:Button ID="BtnLight2" Text="Light 2 : -" runat="server" OnClick="BtnLight2_Click" />
				<asp:Button ID="BtnLight3" Text="Light 3 : -" runat="server" OnClick="BtnLight3_Click" />
				<asp:Button ID="BtnLight4" Text="Light 4 : -" runat="server" OnClick="BtnLight4_Click" />
			</div>
		</div>
	</form>
</body>
</html>
