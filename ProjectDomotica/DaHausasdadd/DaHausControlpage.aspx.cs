﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica.DaHaus {
	public partial class DaHausControlpage : System.Web.UI.Page {
		string permissionString = "";
		protected void Page_Load(object sender, EventArgs e) {
			string userID = "";
			HttpCookie cookie = Request.Cookies["SessionData"];
			if(cookie == null) {
				Server.Transfer("DaHausLogin.aspx");
			} else {
				userID = cookie["UserID"];
				HttpCookie newCookie = new HttpCookie("SessionData");
				newCookie["UserID"] = userID;
				newCookie.Expires = DateTime.Now.AddDays(2);
				Response.Cookies.Add(newCookie);
			}
			

			LblWelcomeMessage.Text = "Welcome to the DaHaus control page " + userID;
			MySqlDataReader sqlReader = Global.SqlConnection.executeSQLCommand(String.Format("SELECT PermissionString FROM dahauspermissions WHERE UserID = {0};", userID));
			if (sqlReader.HasRows) {
				sqlReader.Read();

				permissionString = sqlReader.GetString(0);

				sqlReader.Close();
			}
			LblWelcomeMessage.Text += "<br />Your permissions are " + permissionString;
			updateLights();
 		}

		protected void BtnLight0_Click(object sender, EventArgs e) {
			if (permissionString.Substring(0, 1).Equals("2")) {
				Global.DaHaus.toggleLight("0");
			}
		}

		protected void BtnLight1_Click(object sender, EventArgs e) {
			if (permissionString.Substring(1, 1).Equals("2")) {
				Global.DaHaus.toggleLight("1");
			}
		}

		protected void BtnLight2_Click(object sender, EventArgs e) {
			if (permissionString.Substring(2, 1).Equals("2")) {
				Global.DaHaus.toggleLight("2");
			}
		}

		protected void BtnLight3_Click(object sender, EventArgs e) {
			if (permissionString.Substring(3, 1).Equals("2")) {
				Global.DaHaus.toggleLight("3");
			}
		}

		protected void BtnLight4_Click(object sender, EventArgs e) {
			if (permissionString.Substring(4, 1).Equals("2")) {
				Global.DaHaus.toggleLight("4");
			}
		}

		protected void updateLights() {
			if (permissionString.Substring(0, 1).Equals("2") || permissionString.Substring(0, 1).Equals("1")) {
				BtnLight0.Text = "Light 0: " + (Global.DaHaus.lights[0] ? "On" : "Off");
			}
			if (permissionString.Substring(1, 1).Equals("2") || permissionString.Substring(1, 1).Equals("1")) {
				BtnLight1.Text = "Light 1: " + (Global.DaHaus.lights[1] ? "On" : "Off");
			}
			if (permissionString.Substring(2, 1).Equals("2") || permissionString.Substring(2, 1).Equals("1")) {
				BtnLight2.Text = "Light 2: " + (Global.DaHaus.lights[2] ? "On" : "Off");
			}
			if (permissionString.Substring(3, 1).Equals("2") || permissionString.Substring(3, 1).Equals("1")) {
				BtnLight3.Text = "Light 3: " + (Global.DaHaus.lights[3] ? "On" : "Off");
			}
			if (permissionString.Substring(4, 1).Equals("2") || permissionString.Substring(4, 1).Equals("1")) {
				BtnLight4.Text = "Light 4: " + (Global.DaHaus.lights[4] ? "On" : "Off");
			}
		}
	}
}