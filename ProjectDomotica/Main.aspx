﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="ProjectDomotica.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Domotica Webapp</title>
    <!-- Latest compiled and minified CSS -->
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />


    <link rel="stylesheet" type="text/css" href="CSS/Main.css" runat="server" />

    <!-- Latest compiled and minified JavaScript -->
   
    <script src="JavaScript/Grid.js"></script>
    <script src="JavaScript/Screen.js"></script>
</head>
<body id="globalheader" onresize="setSizeToHiddenObj()" runat="server">
    <input type="hidden" runat="server" id="hiddenValue" />
    <form id="form1" runat="server">
        <div>
            <nav>
                <ul class="nav nav-pills" role="navigation">
                    <li role="presentation" class="active"><a href="#">Home</a></li>
                    <li role="presentation"><a href="#">Profile</a></li>
                    <li role="presentation"><a href="#">Messages</a></li>
                </ul>
            </nav>

            <div id="GridContainer" class="editMode" runat="server">
            </div>

            <asp:Label ID="lbl_test" runat="server" Text="Label"></asp:Label>

        </div>
    </form>
</body>
</html>
