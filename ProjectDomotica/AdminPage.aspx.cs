﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectDomotica {
	public partial class AdminPage : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (Session["UserID"] == null) {
				Response.Redirect("/MasterPageTest.aspx");
			}
			if (!Global.isAdmin((int)Session["UserID"])) {
				Response.Redirect("/MasterPageTest.aspx");
			}

			MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("SELECT users.userid, username, isAdmin FROM users LEFT JOIN permissions ON permissions.userid = users.userid"));

			if (reader.HasRows) {
				while (reader.Read()) {
					ListItem item = new ListItem();
					item.Text = reader.GetString(1);
					item.Value = reader.GetInt32(0).ToString();
					if (!reader.IsDBNull(2) && reader.GetInt32(2) != 0) {
						AdminBox.Items.Add(item);
					} else {
						NonAdminBox.Items.Add(item);
					}
				}
			}

			reader.Close();

			reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("SELECT userid, username FROM users;"));
			if (reader.HasRows) {
				while (reader.Read()) {
					ListItem item = new ListItem();
					item.Text = reader.GetString(1);
					item.Value = reader.GetInt32(0).ToString();
					users.Items.Add(item);
				}
			}
			reader.Close();
		}

		protected void applyButton_Click(object sender, EventArgs e) {
			ListItemCollection items = NonAdminBox.Items;
			foreach (ListItem item in items) {
				if (item.Attributes["changed"] != null && item.Attributes["changed"].Equals("true")) {
					MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("UPDATE permissions SET isAdmin = 0 WHERE userid = " + item.Value + ";"));
					reader.Close();
				}
			}
			items = AdminBox.Items;
			foreach (ListItem item in items) {
				if (item.Attributes["changed"] != null && item.Attributes["changed"].Equals("true")) {
					MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("UPDATE permissions SET isAdmin = 0 WHERE userid = " + item.Value + ";"));
					if (reader.RecordsAffected > 0) {
						reader.Close();
						return;
					} else {
						reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("INSERT INTO permissions (userid, isAdmin) VALUES (" + item.Value + ", 1);"));
						reader.Close();
					}
				}
			}
		}

		protected void BtnToAdmin_Click(object sender, EventArgs e) {
			NonAdminBox.SelectedValue = Request.Form[NonAdminBox.UniqueID];
			ListItem item = NonAdminBox.SelectedItem;
			if (item != null) {
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("UPDATE permissions SET isAdmin = 0 WHERE userid = " + item.Value + ";"));
				if (reader.RecordsAffected > 0) {
					reader.Close();
					return;
				} else {
					reader.Close();
					reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("INSERT INTO permissions (userid, isAdmin) VALUES (" + item.Value + ", 1);"));
					reader.Close();
				}
			}
			Response.Redirect("/AdminPage.aspx");
		}

		protected void BtnToNonAdmin_Click(object sender, EventArgs e) {
			AdminBox.SelectedValue = Request.Form[AdminBox.UniqueID];
			ListItem item = AdminBox.SelectedItem;
			if (item != null) {
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(new MySqlCommand("UPDATE permissions SET isAdmin = 0 WHERE userid = " + item.Value + ";"));
				reader.Close();
			}
			Response.Redirect("/AdminPage.aspx");
		}

		protected void applyBalance_Click(object sender, EventArgs e) {
			users.SelectedValue = Request.Form[users.UniqueID];
			ListItem item = users.SelectedItem;

			if (item != null) {
				MySqlCommand command = new MySqlCommand("UPDATE userdata SET balance = ? WHERE userid = " + item.Value);
				command.Parameters.Add(new MySqlParameter("balance", newBalance.Text));
				MySqlDataReader reader = Global.SqlConnection.executeSQLCommand(command);
				if (reader.RecordsAffected > 0) {
					reader.Close();
				} else {
					reader.Close();
					command = new MySqlCommand("INSERT INTO userdata(userid, balance) VALUES(" + item.Value + ", ?);");
					command.Parameters.Add(new MySqlParameter("balance", newBalance.Text));
					reader = Global.SqlConnection.executeSQLCommand(command);
					reader.Close();
				}
			}
		}
	}
}